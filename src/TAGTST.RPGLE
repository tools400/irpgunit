     /**
      * Tag service program as unit test
      *
      * The user defined attribute of the object description will be set
      * to RPGUNIT to mark the service program as a unit test. The
      * text value will be left as it is (probably blank).
      *
      * @author Mihael Schmidt
      * @date 2010-10-01
      */

      /copy COPYRIGHT.RPGINC

      /copy TEMPLATES.RPGINC
      /copy ERRORCODE.RPGINC
      /copy SYSTEMAPI.RPGINC
      /copy TAGTST.RPGINC


     D main            PR
     D   object                      10A   const
     D   library                     10A   const

       //----------------------------------------------------------------------
       //   PEP
       //----------------------------------------------------------------------
     D RUTAGTST...
     D                 PI
     D   object                      10A   const
     D   library                     10A   const

       main(object : library);
       *inlr = *on;


     P main...
     P                 B
     D                 PI
     D   object                      10A   const
     D   library                     10A   const
      *
     D lib             S             10A

     D objInfo         ds                  qualified
     D   keys                        10I 0
     D   type                        10I 0
     D   length                      10I 0
     D   attribute                   30A

      /free

       clear errorCode;
       errorCode.bytesP = 0;

       // Change: user defined attribute
       objInfo.keys = 1;
       objInfo.type = 9;
       objInfo.length = 10;
       objInfo.attribute = 'RPGUNIT';
       QLICOBJD(lib : object + library : '*SRVPGM' : objInfo : errorCode);

      /end-free
     P                 E
