#-------------------------------------------------------------------------------
# User-defined part start
#

# BIN_LIB is the destination library for the service program.
# The rpg modules and the binder source file are also created in BIN_LIB.
# Binder source file and rpg module can be remove with the clean step 
# (make clean).
SRC_DIR=irpgunit
BIN_LIB=RPGUNIT
SAVE_FILE=RPGUNIT
SRC_SAVE_FILE=SOURCES

#
# The folder where the copy books will be copied to with the install step (make install).
#
USRINCDIR='/usr/local/include'  

#
# Target Release
#
TGTRLS=*CURRENT

#
# User-defined part end
#-------------------------------------------------------------------------------

BINDFLAGS=ACTGRP(*CALLER) STGMDL(*INHERIT) BNDDIR($(BIN_LIB)/IRPGUNIT) BNDSRVPGM($(BIN_LIB)/RUTESTCASE) TGTRLS($(TGTRLS))

all: env compile bind cmd

env:
	-system -qi "CRTLIB $(BIN_LIB) TYPE(*TEST) TEXT('iRPGUnit : Unit Testing Framework')                                          
	-system -qi "CRTBNDDIR BNDDIR($(BIN_LIB)/IRPGUNIT)"
	-system -qi "ADDBNDDIRE BNDDIR($(BIN_LIB)/IRPGUNIT) OBJ(($(BIN_LIB)/LLIST *MODULE))"
	-system -qi "ADDBNDDIRE BNDDIR($(BIN_LIB)/IRPGUNIT) OBJ(($(BIN_LIB)/STRING *MODULE))"
	-system -qi "ADDBNDDIRE BNDDIR($(BIN_LIB)/IRPGUNIT) OBJ(($(BIN_LIB)/USRSPC *MODULE))"
	-system -qi "ADDBNDDIRE BNDDIR($(BIN_LIB)/IRPGUNIT) OBJ(($(BIN_LIB)/SRCMBR *MODULE))"
	-system -qi "ADDBNDDIRE BNDDIR($(BIN_LIB)/IRPGUNIT) OBJ(($(BIN_LIB)/RMTRUNSRV *MODULE))"
	-system -qi "ADDBNDDIRE BNDDIR($(BIN_LIB)/IRPGUNIT) OBJ(($(BIN_LIB)/PGMMSG *MODULE))"
	-system -qi "ADDBNDDIRE BNDDIR($(BIN_LIB)/IRPGUNIT) OBJ(($(BIN_LIB)/LIBL *MODULE))"
	-system -qi "ADDBNDDIRE BNDDIR($(BIN_LIB)/IRPGUNIT) OBJ(($(BIN_LIB)/EXTTST *MODULE))"
	-system -qi "ADDBNDDIRE BNDDIR($(BIN_LIB)/IRPGUNIT) OBJ(($(BIN_LIB)/EXTPRC *MODULE))"
	-system -qi "ADDBNDDIRE BNDDIR($(BIN_LIB)/IRPGUNIT) OBJ(($(BIN_LIB)/CMDRUNSRV *MODULE))"
	-system -qi "ADDBNDDIRE BNDDIR($(BIN_LIB)/IRPGUNIT) OBJ(($(BIN_LIB)/CMDRUNPRT *MODULE))"
	-system -qi "ADDBNDDIRE BNDDIR($(BIN_LIB)/IRPGUNIT) OBJ(($(BIN_LIB)/CMDRUNLOG *MODULE))"
	-system -qi "ADDBNDDIRE BNDDIR($(BIN_LIB)/IRPGUNIT) OBJ(($(BIN_LIB)/CALLPROC *MODULE))"
	-system -qi "ADDBNDDIRE BNDDIR($(BIN_LIB)/IRPGUNIT) OBJ(($(BIN_LIB)/CMDRUN *MODULE))"
	-system -qi "ADDBNDDIRE BNDDIR($(BIN_LIB)/IRPGUNIT) OBJ(($(BIN_LIB)/CRTTST *MODULE))"
	-system -qi "ADDBNDDIRE BNDDIR($(BIN_LIB)/IRPGUNIT) OBJ(($(BIN_LIB)/XMLWRITER *MODULE))"
	-system -qi "DLTF FILE($(BIN_LIB)/RPGUNIT1)"
	-system -qi "CRTSRCPF FILE($(BIN_LIB)/RPGUNIT1) RCDLEN(112)"
	-system -i "CPYFRMSTMF FROMSTMF('include/TESTCASE.RPGINC') TOMBR('/QSYS.LIB/$(BIN_LIB).LIB/RPGUNIT1.FILE/TESTCASE.MBR') MBROPT(*replace)"
	-system -i "CPYFRMSTMF FROMSTMF('src/RUWSCST.TXT') TOMBR('/QSYS.LIB/$(BIN_LIB).LIB/RPGUNIT1.FILE/RUWSCST.MBR') MBROPT(*replace)"
	-system -i "CRTWSCST WSCST($(BIN_LIB)/RUWSCST) SRCFILE($(BIN_LIB)/RPGUNIT1)"
	-system -i "RMVM FILE($(BIN_LIB/RPGUNIT1) MBR(RUWSCST)"
	-system -i "CRTJOBD JOBD($(BIN_LIB)/RPGUNIT) INLLIBL($(BIN_LIB)) TEXT('iRPGUnit - Library list for test cases')"

compile: .PHONY llist
	cd src && make BIN_LIB=$(BIN_LIB)

llist: .PHONY
	cd llist && make BIN_LIB=$(BIN_LIB)

bind: .PHONY
	-system -qi "DLTOBJ OBJ($(BIN_LIB)/RUTESTCASE) OBJTYPE(*SRVPGM)"
	system -ki "CRTSRVPGM SRVPGM($(BIN_LIB)/RUTESTCASE) MODULE($(BIN_LIB)/ASSERT $(BIN_LIB)/PGMMSG $(BIN_LIB)/TESTUTILS $(BIN_LIB)/VERSION) SRCSTMF('src/RUTESTCASE.BND') TEXT('iRPGUnit : Testcase Procedures') ACTGRP(*CALLER) STGMDL(*INHERIT) BNDDIR($(BIN_LIB)/IRPGUNIT) TGTRLS($(TGTRLS))"
	-system -qi "DLTOBJ OBJ($(BIN_LIB)/RUCRTTST) OBJTYPE(*PGM)"
	system -ki "CRTPGM $(BIN_LIB)/RUCRTTST MODULE($(BIN_LIB)/CRTTST) $(BINDFLAGS)"
	-system -qi "DLTOBJ OBJ($(BIN_LIB)/RUTAGTST) OBJTYPE(*PGM)"
	system -ki "CRTPGM $(BIN_LIB)/RUTAGTST MODULE($(BIN_LIB)/TAGTST) $(BINDFLAGS)"
	-system -qi "DLTOBJ OBJ($(BIN_LIB)/RUCALLTST) OBJTYPE(*PGM)"
	system -ki "CRTPGM $(BIN_LIB)/RUCALLTST MODULE($(BIN_LIB)/CMDRUN) ACTGRP(*NEW) BNDDIR($(BIN_LIB)/IRPGUNIT) BNDSRVPGM($(BIN_LIB)/RUTESTCASE) TGTRLS($(TGTRLS))"
	-system -qi "DLTOBJ OBJ($(BIN_LIB)/RUCALLXML) OBJTYPE(*PGM)"
	system -ki "CRTPGM $(BIN_LIB)/RUCALLXML MODULE($(BIN_LIB)/CMDRUNXML) ACTGRP(*NEW) BNDDIR($(BIN_LIB)/IRPGUNIT) BNDSRVPGM($(BIN_LIB)/RUTESTCASE) TGTRLS($(TGTRLS))"
	-system -qi "DLTOBJ OBJ($(BIN_LIB)/RUPGMRMT) OBJTYPE(*PGM)"
	system -ki "CRTPGM $(BIN_LIB)/RUPGMRMT MODULE($(BIN_LIB)/PGMRMT) ACTGRP(*NEW) BNDDIR($(BIN_LIB)/IRPGUNIT) BNDSRVPGM($(BIN_LIB)/RUTESTCASE) TGTRLS($(TGTRLS))"
	system -ki "CRTSRVPGM $(BIN_LIB)/RURUNRMT MODULE($(BIN_LIB)/RMTRUNSRV) $(BINDFLAGS) EXPORT(*ALL)"

cmd: .PHONY
	cd cmd && make BIN_LIB=$(BIN_LIB)

clean:
	-system -qi "DLTBNDDIR $(BIN_LIB)/IRPGUNIT"
	-system -qi "DLTPGM $(BIN_LIB)/RUCRTTST"
	-system -qi "DLTPGM $(BIN_LIB)/RUPGMRMT"
	-system -qi "DLTPGM $(BIN_LIB)/RUTAGTST"
	-system -qi "DLTPGM $(BIN_LIB)/RUCALLTST"
	-system -qi "DLTPGM $(BIN_LIB)/RUCALLXML"
	-system -qi "DLTSRVPGM $(BIN_LIB)/RURUNRMT"
	-system -qi "DLTSRVPGM $(BIN_LIB)/RUTESTCASE"
	-system -qi "DLTJOBD $(BIN_LIB)/RPGUNIT"
	-system -qi "DLTWSCST $(BIN_LIB)/RUWSCST"
	-system -qi "DLTF $(BIN_LIB)/IRPGUNIT"
	-system -qi "DLTF FILE($(BIN_LIB)/RPGUNIT1)"
	
	cd src && make BIN_LIB=$(BIN_LIB) clean
	cd cmd && make BIN_LIB=$(BIN_LIB) clean
	cd llist && make BIN_LIB=$(BIN_LIB) clean
	cd unittest && make BIN_LIB=$(BIN_LIB) clean

test: .PHONY
	cd unittest && make BIN_LIB=$(BIN_LIB)

package: .PHONY
	-system -i "DLTF $(BIN_LIB)/$(SRC_SAVE_FILE)"
	system -i "CRTSAVF $(BIN_LIB)/$(SRC_SAVE_FILE)"
	system -i "SAV DEV('/qsys.lib/$(BIN_LIB).lib/$(SRC_SAVE_FILE).file') OBJ(('../$(SRC_DIR)')) PATTERN(('.git' *OMIT) ('.project' *OMIT)) SUBTREE(*ALL)"
	-system -i "DLTF $(BIN_LIB)/$(SAVE_FILE)"
	system -i "CRTSAVF $(BIN_LIB)/$(SAVE_FILE)"
	system -i "SAVLIB LIB($(BIN_LIB)) DEV(*SAVF) SAVF($(BIN_LIB)/$(SAVE_FILE)) TGTRLS($(TGTRLS)) DTACPR(*HIGH) \
              SELECT( \
                (*INCLUDE $(BIN_LIB)/RUCALLTST  *PGM) \
                (*INCLUDE $(BIN_LIB)/RUCRTTST   *PGM) \
                (*INCLUDE $(BIN_LIB)/RUPGMRMT   *PGM) \
                (*INCLUDE $(BIN_LIB)/RUTAGTST   *PGM) \
                (*INCLUDE $(BIN_LIB)/RURUNRMT   *SRVPGM) \
                (*INCLUDE $(BIN_LIB)/RUTESTCASE *SRVPGM) \
                (*INCLUDE $(BIN_LIB)/RPGUNIT1   *FILE) \
                (*INCLUDE $(BIN_LIB)/RPGUNIT    *JOBD) \
                (*INCLUDE $(BIN_LIB)/RUCALLTST  *CMD)  \
                (*INCLUDE $(BIN_LIB)/RUCRTTST   *CMD)  \
                (*INCLUDE $(BIN_LIB)/RUTAGTST   *CMD)  \
                (*INCLUDE $(BIN_LIB)/RUCMDHLP   *PNLGRP) \
                (*INCLUDE $(BIN_LIB)/RUWSCST    *WSCST) \
                (*INCLUDE $(BIN_LIB)/$(SRC_SAVE_FILE)  *FILE) \
              )"
	
install:
	-mkdir $(USRINCDIR)/irpgunit
	cp include/ASSERT.RPGINC $(USRINCDIR)/irpgunit/
	cp include/TESTUTILS.RPGINC $(USRINCDIR)/irpgunit/
	ln -s $(USRINCDIR)/irpgunit/ASSERT.RPGINC $(USRINCDIR)/irpgunit/assert

uninstall:
	-rm -rf $(USRINCDIR)/irpgunit

.PHONY:

